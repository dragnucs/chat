<!DOCTYPE html>
<html>
    <head>
        <title>Hiit Co. T'chat</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link media="all" rel="stylesheet" href="/css/bootstrap.min.css" />
        <link media="all" rel="stylesheet" href="/css/main.css" />
    </head>
    <body>
        <nav class="navbar navbar-expand fixed-top navbar-dark bg-primary">
            <a class="navbar-brand" href="/">Hiit Co. t'chat</a>
            <ul class="navbar-nav">
                <li class="nav-item pull-right">
                    <a class="nav-link" href="/login">Login</a>
                </li>
            </ul>
        </nav>
        <div class="container-fluid text-center">
            <form class="form-signin" action="/register" method="POST">
                <h1 class="h3 mb-3 font-weight-normal">Resigter</h1>

                <?php if (isset($errors)): ?>
                    <?php foreach ($errors as $error): ?>
                        <div class="alert alert-danger" role="alert">
                            <?= $error ?>
                        </div>
                    <?php endforeach ?>
                <?php endif ?>

                <label for="username" class="sr-only">Username</label>
                <input type="text" id="username" name="username" class="form-control" placeholder="Username" required autofocus>
                <label for="password" class="sr-only">Password</label>
                <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
                <label for="confirmation" class="sr-only">Password</label>
                <input type="password" id="confirmation" name="confirmation" class="form-control" placeholder="Password" required>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
            </form>

        </div>
        <script crossorigin="anonymous" type="application/javascript" src="/js/main.js"></script>
    </body>
</html>
