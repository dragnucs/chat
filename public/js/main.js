function reload () {
    console.log('reloading')
    window.location.reload()
}

let timeout = setTimeout(reload, 5*1000)

const body = document.querySelector('#body')

body.onkeypress = function () {
    clearTimeout(timeout)

    timeout = setTimeout(reload, 5*1000)
}
