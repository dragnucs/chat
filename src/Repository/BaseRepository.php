<?php

namespace App\Repository;

use App\Database\PDO;

/**
 * Base repository class to instanciate with a PDO.
 */
class BaseRepository
{
    protected $pdo;

    public function __construct()
    {
        $this->pdo = (new PDO())->getPdo();
    }
}
