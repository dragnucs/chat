<?php

namespace App\Repository;

use App\Model\Message;
use App\Model\User;

/**
 * Message Repository.
 */
class MessageRepository extends BaseRepository
{

    /**
     * Find all messages.
     */
    public function findAll()
    {
        $statement = $this->pdo->query(
            'SELECT id, owner_id, recipient_id, time, body
               FROM message'
        );

        $userRepository = new UserRepository();

        return array_map(function ($message) use ($userRepository){
            return (new Message())
                ->setId($message['id'])
                ->setOwner($userRepository->find($message['owner_id']))
                ->setRecipient($userRepository->find($message['recipient_id']))
                ->setTime(new \DateTime($message['time']))
                ->setBody($message['body']);
        }, $statement->fetchAll());
    }

    /**
     * Find messages in a chat room.
     */
    public function findRoom(User $owner, User $recipient)
    {
        $statement = $this->pdo->prepare(
            'SELECT id, owner_id, recipient_id, time, body
               FROM message
              WHERE (owner_id = :owner_id AND recipient_id = :recipient_id)
                 OR (owner_id = :inverted_recipient_id AND recipient_id = :inverted_owner_id)
           ORDER BY time ASC'
        );

        $statement->execute([
            ':owner_id' => $owner->getId(),
            ':recipient_id' => $recipient->getId(),
            ':inverted_owner_id' => $owner->getId(),
            ':inverted_recipient_id' => $recipient->getId(),
        ]);

        return array_map(function ($message) use ($owner, $recipient){
            return (new Message())
                ->setId($message['id'])
                ->setOwner($message['owner_id'] === $owner->getId() ? $owner : $recipient)
                ->setRecipient($message['recipient_id'] === $owner->getId() ? $owner : $recipient)
                ->setTime(new \DateTime($message['time']))
                ->setBody($message['body']);
        }, $statement->fetchAll());
    }

    /**
     * Persist a message to the database.
     */
    public function persist(Message $message)
    {
        $statement = $this->pdo->prepare(
            'INSERT INTO message (owner_id, recipient_id, time, body)
                  VALUES (:owner_id, :recipient_id, :time, :body)'
        );

        $statement->execute([
            ':owner_id' => $message->getOwner()->getId(),
            ':recipient_id' => $message->getRecipient()->getId(),
            ':time' => $message->getTime()->format('Y-m-d H:i:s'),
            ':body' => $message->getBody(),
        ]);

        return $message->setId($this->pdo->lastInsertId());
    }
}
