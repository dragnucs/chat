<?php

namespace App\Model;

use App\Model\User;

/**
 * Message object model.
 */
class Message
{
    private $id;
    private $owner;
    private $recipient;
    private $time;
    private $body;

    public function __construct()
    {
        $this->setTime(new \DateTime());
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Message
    {
        if (null === $this->id) {
            $this->id = $id;
        }

        return $this;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function setBody(?string $body): Message
    {
        $this->body = $body;

        return $this;
    }

    public function getTime(): \DateTime
    {
        return $this->time;
    }

    public function setTime(\DateTime $time): Message
    {
        $this->time = $time;

        return $this;
    }

    public function getOwner(): User
    {
        return $this->owner;
    }

    public function setOwner(User $owner): Message
    {
        $this->owner = $owner;

        return $this;
    }

    public function setRecipient(User $recipient): Message
    {
        $this->recipient = $recipient;

        return $this;
    }

    public function getRecipient(): User
    {
        return $this->recipient;
    }
}
