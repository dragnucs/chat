<?php
session_start();

require __DIR__.'/../vendor/autoload.php';

use App\Router;

$router = new Router();

$router->use(function () {
    if (!$_SESSION['logged']
        && !in_array($_SERVER['REQUEST_URI'], ['/login', '/register'])
    ) {
        return header('Location: /login');
    }
});

$router
    ->get('/', ['App\Controller\RoomController', 'index'])

    ->get('/register', ['App\Controller\UserController', 'register'])
    ->post('/register', ['App\Controller\UserController', 'create'])

    ->get('/chat', ['App\Controller\RoomController', 'rooms'])
    ->post('/chat', ['App\Controller\RoomController', 'send'])

    ->get('/login', ['App\Controller\UserController', 'login'])
    ->get('/logout', ['App\Controller\UserController', 'logout'])
    ->post('/authenticate', ['App\Controller\UserController', 'authenticate'])
;

$router->dispatch();
