<?php

namespace App\Controller;

use App\Model\Message;
use App\Model\User;
use App\Repository\UserRepository;
use App\Repository\MessageRepository;
use App\Traits\Templatable;

/**
 * Chat rooms controller.
 */
class RoomController
{
    use Templatable;

    /**
     * Chat rooms index when no actual chat is selected.
     */
    public function index(): string
    {
        $userRepository = new UserRepository();

        $users = $userRepository->findAll();
        $owner = $userRepository->find($_SESSION['user_id']);

        return $this->render('room/index.html.php', [
            'users' => $users,
            'user' => $owner,
        ]);
    }

    /**
     * List chat rooms.
     *
     * Get the list of chat rooms that are basicaly the registered users.
     */
    public function rooms(): string
    {
        $userRepository = new UserRepository();
        $messageRepository = new MessageRepository();

        $owner = $userRepository->find($_SESSION['user_id']);
        $recipient = $userRepository->find($_GET['w']);

        $users = $userRepository->findAll();
        $messages = $messageRepository->findRoom($owner, $recipient);

        return $this->render('room/chat.html.php', [
            'users' => $users,
            'messages' => $messages,
            'user' => $owner,
        ]);
    }

    /**
     * Send a message to a recipient.
     */
    public function send(): string
    {
        $userRepository = new UserRepository();
        $messageRepository = new MessageRepository();

        $owner = $userRepository->find($_SESSION['user_id']);
        $recipient = $userRepository->find($_GET['w']);

        $message = (new Message())
                 ->setOwner($owner)
                 ->setRecipient($recipient)
                 ->setBody(htmlspecialchars($_POST['body']))
                 ;

        $messageRepository->persist($message);

        header('Location: /chat?w='.$_GET['w']);

        return '';
    }
}
