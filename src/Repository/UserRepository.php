<?php

namespace App\Repository;

use App\Model\User;

/**
 * User repository.
 */
class UserRepository extends BaseRepository
{
    /**
     * Find all users.
     */
    public function findAll(): array
    {
        $statement = $this->pdo->query(
            'SELECT id, username, lastseen
               FROM user'
        );

        return array_map(function ($user) {
            return (new User())
                ->setId($user['id'])
                ->setUsername($user['username'])
                ->setLastseen($user['lastseen']);
        }, $statement->fetchAll());
    }

    /**
     * Find a single user by ID.
     */
    public function find(int $id): User
    {
        $statement = $this->pdo->prepare(
            'SELECT id, username, lastseen
               FROM user
              WHERE user.id = :id
              LIMIT 1'
        );

        $statement->execute([
            ':id' => $id,
        ]);

        $user = $statement->fetchAll()[0];

        return (new User())
            ->setId($user['id'])
            ->setUsername($user['username'])
            ->setLastseen($user['lastseen']);
    }

    /**
     * Checks if the user trying to authenticate has the correct credentials.
     *
     * @retuens int|null Returns the ID if credentials check is successful or null otherwise.
     */
    public function checkCredentials(string $username, string $password): ?int
    {
        $statement = $this->pdo->prepare(
            'SELECT id, password
               FROM user
              WHERE user.username = :username
              LIMIT 1'
        );

        $statement->execute([
            ':username' => $username
        ]);

        $user = $statement->fetchAll()[0];

        return password_verify($password, $user['password']) ? $user['id'] : null;
    }

    /**
     * Persists a user to the database.
     *
     * @return User the new user with ID populated.
     */
    public function persist(User $user): User
    {
        $statement = $this->pdo->prepare(
            'INSERT INTO user (username, password)
                  VALUES (:username, :password)'
        );

        $statement->execute([
            ':username' => $user->getUsername(),
            ':password' => $user->getHashedPassword(),
        ]);

        return $user->setId($this->pdo->lastInsertId());
    }
}
