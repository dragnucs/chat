<?php

namespace App\Command;

use App\Database\PDO;

/**
 * Drops all tables create during initial seed.
 */
class DatabaseTeardown
{
    /**
     * Teardown quries command execution.
     */
    public static function execute()
    {
        $pdo = (new PDO())->getPdo();

        $pdo->query('DROP TABLE message');
        $pdo->query('DROP TABLE user');
    }
}
