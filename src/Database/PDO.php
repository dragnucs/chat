<?php

namespace App\Database;

/**
 * Database layer.
 */
class PDO
{
    private $pdo;

    /**
     * Setup the PDO.
     *
     * PDO settings are loaded from environment with fallback default values.
     */
    public function __construct()
    {
        $host = getenv('DATABASE_HOST') ?: '127.0.0.1';
        $name = getenv('MYSQL_DATABASE') ?: 'database';
        $user = getenv('MYSQL_USER') ?: 'user';
        $pass = getenv('MYSQL_PASSWORD') ?: 'password';

        $dsn = "mysql:host=$host;dbname=$name;charset=utf8mb4";

        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES => false,
        ];

        try {
            return $this->pdo = new \PDO($dsn, $user, $pass, $options);
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
    }

    /**
     * Returns the PDO instance.
     *
     * Note that PDO here is not static.
     */
    public function getPdo(): \PDO
    {
        return $this->pdo;
    }
}
