<?php

namespace App\Controller;

use App\Model\User;
use App\Repository\UserRepository;
use App\Traits\Templatable;

/**
 * User controller.
 */
class UserController
{
    use Templatable;

    /**
     * Create a user in the database.
     */
    public function create(): string
    {
        $errors = [];

        $username = htmlspecialchars($_POST['username']);
        $password = $_POST['password'];
        $confirmation = htmlspecialchars($_POST['confirmation']);

        if (empty($username)) {
            $errors[] = 'Invalid username';
        }

        if (empty($password) || strlen($password) < 8) {
            $errors[] = 'Invalid password';
        }

        if ($password !== $confirmation) {
            $errors[] = 'Password and confirmation do not match';
        }

        if (count($errors)) {
            return $this->render('user/register.html.php', [
                'errors' => $errors,
            ]);
        }

        $user = (new User())
              ->setUsername($username)
              ->setHashedPassword($password);

        $userRepository = new UserRepository();
        $userRepository->persist($user);

        header('Location: /login');

        return '';
    }

    /**
     * Authenticate a user agains the users list.
     */
    public function authenticate(): string
    {
        $username = htmlspecialchars($_POST['username']);
        $password = $_POST['password'];

        $userRepository = new UserRepository();
        $userId = $userRepository->checkCredentials($username, $password);

        if (null !== $userId) {
            $user = $userRepository->find($userId);

            $_SESSION['logged'] = true;
            $_SESSION['user_id'] = $user->getId();

            header('Location: /');

            return '';
        }

        $errors = [
            'Login error. Please try again.',
        ];

        return $this->render('user/login.html.php', [
            'errors' => $errors,
        ]);
    }

    /**
     * Register new user.
     */
    public function register(): string
    {
        return $this->render('user/register.html.php');
    }

    /**
     * Login for existing users.
     */
    public function login(): string
    {
        return $this->render('user/login.html.php');
    }

    /**
     * Logout existing users and clear sessions.
     */
    public function logout(): string
    {
        session_destroy();

        header('Location: /login');

        return '';
    }
}
