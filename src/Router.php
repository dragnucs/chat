<?php

namespace App;

/**
 * Router to dispatch HTTP queries to controller methods.
 */
class Router
{
    private $routes;
    private $middlewares;

    /**
     * A route.
     *
     * @param string $route The route path
     * @param string $method HTTP method
     * @param array $action An array as excpected by call_user_func
     */
    public function add(string $route, string $method, array $action): Router
    {
        if (!in_array($method, ['GET', 'POST'])) {
            throw new \Exception('Invalid route method '.$method);
        }

        $this->routes[$route][$method] = $action;

        return $this;
    }

    /**
     * Shortt cust to `add` a GET route.
     */
    public function get(string $route, array $action): Router
    {
        return $this->add($route, 'GET', $action);
    }

    /**
     * Shortt cust to `add` a POST route.
     */
    public function post(string $route, array $action): Router
    {
        return $this->add($route, 'POST', $action);
    }

    /**
     * Add a middleware to use for the router.
     */
    public function use(Callable $middleware): Router
    {
        $this->middlewares[] = $middleware;

        return $this;
    }

    /**
     * Dispatch the actions.
     *
     * First calls all middlewares then calls
     * appropriate controller methods. Route matching
     * is naive and params are passed via GET
     */
    public function dispatch(): void
    {
        $route = explode('?', $_SERVER['REQUEST_URI'])[0];
        $method = $_SERVER['REQUEST_METHOD'];

        $action = $this->routes[$route][$method];

        if (!$action) {
            http_response_code(404);
            echo "Route not found [$method] $route";

            throw new \Exception("Route not found [$method] $route");
        }

        foreach ($this->middlewares as $middleware) {
            call_user_func($middleware);
        }

        $object = new $action[0]();
        $callable = $action[1];

        try {
            echo \call_user_func([$object, $callable]);
        } catch (\Exception $exception) {
            echo $exception;
        }
    }
}
