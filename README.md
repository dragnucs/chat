# Chat

PHP Chat Application interview coding challenge.

Ample documentation in docblocks. Code is meant to be self-explanatory.

## Techniques

Used patterns:

1. MVC Architecture
2. Autoloading
3. Composer commands
4. Router
5. Database layer
6. Object Repository
7. Traits
8. Template rendering
9. Authentication guard middlware
10. Env vars for secrects management
11. Traditional `public` folder with `index.ph` entry file
12. `password_hash` and `password_verify` for secure password storing and matching.

Not used anti-patterns:
1. Service locator
2. Singleton

Not used to keep the project simple: Factory, Real Dependency Inject

**Time constraint workaround:** Instead of building a real time communication
websocket, a simple non intrusive page refresh a has been implemented. The page
refresh does not trigger when the user is typing a message. Communication feels
**near-realtime**.

## Prerequisits

Recommended platform requirements:

1. GNU/Linux
2. PHP 7
3. [Composer](https://getcomposer.org/)
4. MariaDB 10

The project was not tested on other configuration.

## Setup

### TL;DR

``` bash
$ git clone https://gitlab.com/dragnucs/chat.git
$ cd chat
$ vim .env
$ source .env
$ docker-composer up -d # and wait for the database init
$ composer database:seed
$ php -S 0.0.0.0:8000 -t public
```

Visit http://0.0.0.0:8000 in your browser.

### Detailed setup

Clone the repository

``` bash
$ git clone https://gitlab.com/dragnucs/chat.git
```

Edit the `.env` file. For the sack of brievity, the original .env file is
committed to the repository. Edit it with your own settings. Variable
names are self-explanatory. Ensure the settings do refer to existing database
and a user with sufficient priviledges to the database.

If you do not use Docker Compose you will need to load the `.env` variables into
the running session.

``` bash
$ source .env
```

Seed the database.

``` bash
$ composer database:seed
```

This will create the required schema. Please ensure the Database itself and the
user specified in the .env file are properly setup. You might use the provided
`docker-compose.yml` to take care of MariaDB setup.

You can run the project in development mode with the need of Apache or NGINX.

``` bash
$ php -S 0.0.0.0:8000 -t public
```

## Apache/NGINX config

Any traditional Apache or NGINX config should work.

Please [Symfony
docs](https://symfony.com/doc/current/setup/web_server_configuration.html#php-fpm-with-apache-2-2)
about configure up a webserver.

## Notice

The `vendor` directory is committed to the repository to make reviewing easier.
It a regular setup, it should be gitignored.
