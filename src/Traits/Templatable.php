<?php

namespace App\Traits;

/**
 * Templatable trait.
 */
trait Templatable
{
    /**
     * Renders a tempalte.
     *
     * Captures the output of the tempalte using ob_start
     * and returns the result. The template is also fed with
     * values passed as parameter.
     */
    public function render(string $template, array $values = []): string
    {
        $templatePath = __DIR__.'/../View/'.$template;

        if (!file_exists($templatePath)) {
            throw new \Exception('Template not found: '.$templatePath);
        }

        if (is_array($values)) {
            extract($values);
        }

        ob_start();

        include $templatePath;

        return ob_get_clean();
    }
}
