<?php

namespace App\Model;

/**
 * User object model.
 */
class User
{
    private $id;
    private $username;
    private $password;

    /**
     * @var \DateTime $lastseen datime of the last activity of the user. This is more practival than online/offline system.
     */
    private $lastseen;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): User
    {
        if (null === $this->id) {
            $this->id = $id;
        }

        return $this;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): User
    {
        $this->username = $username;

        return $this;
    }

    public function setLastseen(?\DateTime $lastseen): User
    {
        $this->lastseen = $lastseen;

        return $this;
    }

    public function getLastseen(): ?\DateTime
    {
        return $this->lastseen;
    }

    public function setHashedPassword(string $password): User
    {
        $this->password = password_hash($password, PASSWORD_DEFAULT);

        return $this;
    }

    public function getHashedPassword(): string
    {
        return $this->password;
    }

    public function __toString()
    {
        return $this->getUsername();
    }
}
