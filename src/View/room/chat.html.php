<!DOCTYPE html>
<html>
    <head>
        <title>Hiit Co. T'chat</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link media="all" rel="stylesheet" href="/css/bootstrap.min.css" />
        <link media="all" rel="stylesheet" href="/css/main.css" />
    </head>
    <body>
        <nav class="navbar navbar-expand navbar-dark fixed-top bg-primary">
            <a class="navbar-brand" href="/">Hiit Co. t'chat</a>
            <ul class="navbar-nav">
                <li class="nav-item pull-right">
                    <a class="nav-link" href="/logout">Logout</a>
                </li>
            </ul>
            <span class="navbar-text ml-5">Logged as <?= $user->getUsername() ?></span>
        </nav>
        <div class="container-fluid">
            <div class="row">
                <div class="chat-contacts col pl-0">
                    <ul class="list-group list-group-flush border-right">
                        <?php foreach ($users as $user): ?>
                            <li class="list-group-item">
                                <strong>
                                    <a href="/chat?w=<?= $user->getId() ?>"><?= $user->getUsername() ?></a>
                                </strong><br>
                                <time class="time">Active: <?= $user->getLastseen() !== null ? $user->getLastSeen()->format('d M. Y ⋅ h:i') : 'Never' ?></time>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
                <div class="col-10">
                    <div class="messages mt-4">
                        <?php foreach ($messages as $message): ?>
                            <div class="row">
                                <div class="col-2 m-3 text-left">
                                    <strong><?= $message->getOwner() ?></strong>
                                </div>
                                <div class="message col-9 rounded m-1 p-1 pl-2 is-me">
                                    <p class="mb-0"><?= $message->getBody() ?></p>
                                    <time class="time text-muted"><?= $message->getTime()->format('d M. Y ⋅ h:i') ?></time>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                    <form method="POST">
                        <div class="input-group mt-3">
                            <textarea class="form-control" id="body" name="body" aria-label="With textarea" autofocus></textarea>
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script type="application/javascript">
         let messages = document.querySelector('.messages')

         messages.scrollTop = messages.scrollHeight
        </script>
        <script crossorigin="anonymous" type="application/javascript" src="/js/main.js"></script>
    </body>
</html>
