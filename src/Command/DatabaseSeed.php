<?php

namespace App\Command;

use App\Database\PDO;

/**
 * Seed the database.
 */
class DatabaseSeed
{
    /**
     * Executes a seeding query run by `execute` method.
     */
    private static function query(string $table, string $query)
    {
        $pdo = (new PDO())->getPdo();

        try {
            echo "Creating table $table ... ";

            $pdo->query($query);

            echo "OK.\n";
        } catch (\Exception $exception) {
            if ($exception->getCode() === '42S01') {
                echo "Already exists.\n";
            } else {
                echo $exception->getMessage();
            }
        }
    }

    /**
     * Seed quries command execution.
     */
    public static function execute()
    {
        DatabaseSeed::query(
            'user',
            'CREATE TABLE user (
                 id INT PRIMARY KEY AUTO_INCREMENT,
                 username VARCHAR(50) UNIQUE,
                 password VARCHAR(60),
                 lastseen DATETIME );'
        );

        DatabaseSeed::query(
            'message',
            'CREATE TABLE message (
                 id INT PRIMARY KEY AUTO_INCREMENT,
                 owner_id INT,
                 recipient_id INT,
                 time DATETIME,
                 body VARCHAR(500) )'
        );

        DatabaseSeed::query(
            'message',
            'ALTER TABLE message
             ADD CONSTRAINT `fk_message_owner`
                FOREIGN KEY (owner_id) REFERENCES user (id),
             ADD CONSTRAINT `fk_message_recipient`
                FOREIGN KEY (recipient_id) REFERENCES user (id)'
        );
    }
}
